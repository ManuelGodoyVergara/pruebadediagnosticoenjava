package secuenciaFibonacci;

import java.util.Scanner;
import menuPrincipal.menuPrincipal;

public class secuenciaFibonacci {

	public static void secuencia() {
		boolean validacion = false;

		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese un numero mayor a 1");
			Scanner numero = new Scanner(System.in);
			String n = numero.next();
			int fibo1, fibo2;

			if (validar(n) == true) {
				if (Integer.parseInt(n) >= 1) {
					System.out.println("Los " + n + " primeros terminos de la serie de Fibonacci son:");

					fibo1 = 0;
					fibo2 = 1;

					System.out.print(fibo1 + " ");
					for (int i = 0; i < Integer.parseInt(n); i++) {
						System.out.print(fibo2 + " ");

						fibo2 = fibo1 + fibo2;
						fibo1 = fibo2 - fibo1;
					}
					validacion = true;
				} else {
					System.out.println("Ingresar numeros mayores a 1 ");
					validacion = false;
				}

			} else {
				System.out.println("Solo ingresar numeros ");
				validacion = false;
			}
		} while (validacion = !validacion);
		primerMenu();
	}
	
	
	public static void primerMenu() {
		boolean validacion = false ;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Desea volver a menu Principal?");
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 2 donde:");
			System.out.println("1 es un SI");
			System.out.println("2 es un NO y finalizara programa");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					menuPrincipal.main(null);;
					break;
				case 2:
					System.exit(0);
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean el 1 o el 2");
				validacion = false;
			}
			
		}while(validacion = !validacion);
	}
	
	public static boolean validar(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}
}
