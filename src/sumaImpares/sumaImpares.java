package sumaImpares;

import java.util.Scanner;

import menuPrincipal.menuPrincipal;

public class sumaImpares {
	
	
	public static void sumaImpares1() {
		boolean validacion = false;
		int sumaImpares = 0;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese un numero");
			Scanner num = new Scanner(System.in);

			String numero = num.next();

			if (validar(numero) == true) {
				for(int i = 0 ; i<=Integer.parseInt(numero) ; i++){
			        if (i%2 == 1 && i <= Integer.parseInt(numero)){
			            sumaImpares += i;
			            validacion = true;
			        }
			       }
			        System.out.println("La suma de los impares es: "+sumaImpares);
			        primerMenu();
			        
			} else {
				System.out.println("Solo ingresar numeros ");
				validacion = false;
			}
		} while (validacion = !validacion);
		
	}
	
	public static void sumaImpares2() {
		boolean validacion = false;
		int sumaImpares = 0;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			Scanner numero = new Scanner(System.in);
			
	        System.out.println("Ingrese el numero minimo: ");
	        String minimo = numero.next();
	        
	        System.out.println("Ingrese el numero maximo: ");
	        String maximo= numero.next();
	        
			if (validar(minimo) == true && validar(maximo) == true) {
				for(int i = Integer.parseInt(minimo); i<= Integer.parseInt(maximo); i++){
		            if(i%2 == 1 && i <= Integer.parseInt(maximo)){
		                sumaImpares+= i;
		            }
		            primerMenu();
		        }
		        System.out.println("La suma de los impares es: "+sumaImpares );
			} else {
				System.out.println("Solo ingresar numeros ");
				validacion = false;
			}
		} while (validacion = !validacion);
		
	}

	public static void primerMenu() {
		boolean validacion = false ;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Desea volver a menu Principal?");
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 2 donde:");
			System.out.println("1 es un SI");
			System.out.println("2 es un NO y finalizara programa");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					menuPrincipal.main(null);;
					break;
				case 2:
					System.exit(0);
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean el 1 o el 2");
				validacion = false;
			}
			
		}while(validacion = !validacion);
	}
	
	public static boolean validar(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}
}
