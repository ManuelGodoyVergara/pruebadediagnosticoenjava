package menuPrincipal;
import soloPares.soloPares;
import sumaImpares.sumaImpares;
import secuenciaFibonacci.secuenciaFibonacci;
import java.util.Scanner;

public class menuPrincipal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean validacion = false ;
		do {

			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 3 donde:");
			System.out.println("1 es ejercicio solo pares");
			System.out.println("2 es ejercicio suma impares");
			System.out.println("3 es ejercicio secuencia de fibonacci.");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") || numero.equals("3") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					menuEjerciciosPares();
					break;
				case 2:
					menuEjerciciosImpares();
					break;
				case 3:
					secuenciaFibonacci.secuencia();
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean del 1 al 3");
				validacion = false;
			}
			
		}while(validacion = !validacion);
		
	}
	
	public static void menuEjerciciosPares() {
		boolean validacion = false ;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ejercicio Solo Pares");
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 2 donde:");
			System.out.println("1 es ejercicio solo pares parte 1");
			System.out.println("2 es ejercicio solo pares parte 2");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					soloPares.paresParte1();
					break;
				case 2:
					soloPares.paresParte2();
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean el 1 o el 2");
				validacion = false;
			}
			
		}while(validacion = !validacion);
	}
	
	public static void menuEjerciciosImpares() {
		boolean validacion = false ;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ejercicio Suma de Impares");
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 2 donde:");
			System.out.println("1 es ejercicio suma de impares parte 1");
			System.out.println("2 es ejercicio suma de impares parte 2");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					sumaImpares.sumaImpares1();
					break;
				case 2:
					sumaImpares.sumaImpares2();
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean el 1 o el 2");
				validacion = false;
			}
			
		}while(validacion = !validacion);
	}
}
