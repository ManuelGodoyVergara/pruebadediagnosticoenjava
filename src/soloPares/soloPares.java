package soloPares;

import java.util.Scanner;

import menuPrincipal.menuPrincipal;

public class soloPares {

	public static void paresParte1() {
		boolean validacion = false;

		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese un numero");
			Scanner num = new Scanner(System.in);

			String numero = num.next();

			if (validar(numero) == true) {
				for (int i = 0; i < Integer.parseInt(numero); i++) {
					System.out.println(i * 2);
					validacion = true;
				}
				primerMenu();
			} else {
				System.out.println("Solo ingresar numeros ");
				validacion = false;
			}
		} while (validacion = !validacion);
	}

	public static void paresParte2() {
		boolean validacion = false;

		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese un numero");
			Scanner num = new Scanner(System.in);

			String numero = num.next();

			if (validar(numero) == true) {
				for (int i = 1; i < Integer.parseInt(numero) + 1; i++) {
					System.out.println(i * 2);
				}
				primerMenu();
			} else {
				System.out.println("Solo ingresar numeros ");
				validacion = false;
			}
		} while (validacion = !validacion);
		
	}

	
	public static void primerMenu() {
		boolean validacion = false ;
		do {
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Desea volver a menu Principal?");
			System.out.println("-----------------------------------------------------------------------------------------------");
			System.out.println("Ingrese numero de 1 al 2 donde:");
			System.out.println("1 es un SI");
			System.out.println("2 es un NO y finalizara programa");
			System.out.println("-----------------------------------------------------------------------------------------------");
			
			Scanner num = new Scanner(System.in);
			
			String numero = num.next();
			
			if (numero.equals("1") || numero.equals("2") ){
			
				switch (Integer.parseInt(numero)) {
				case 1:
					menuPrincipal.main(null);;
					break;
				case 2:
					System.exit(0);
					break;
				}
				validacion = true;
			}else {
				System.out.println("Solo ingresar numeros enteros que sean el 1 o el 2");
				validacion = false;
			}
			
		}while(validacion = !validacion);
	}
	
	public static boolean validar(String cadena) {
		if (cadena.matches("[0-9]*")) {
			return true;
		} else {
			return false;
		}
	}
}
